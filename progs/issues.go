package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/quadrispro/gotraining/github"
)

var issuesByAgeCategory map[string][]*github.Issue

 func init() {
	issuesByAgeCategory = make(map[string][]*github.Issue)

}

func addIssue(i *github.Issue) {
	now := time.Now()
	oneMonthAgo := now.AddDate(0, -1, 0)
	oneYearAgo := now.AddDate(-1, 0, 0)

	if i.CreatedAt.After(oneYearAgo) {
		issuesByAgeCategory["older than 1yr"] = append(
			issuesByAgeCategory["older than 1yr"], i)
		return
	}
	if i.CreatedAt.Before(oneMonthAgo) {
		issuesByAgeCategory["less than 1mth"] = append(
			issuesByAgeCategory["less than 1mth"], i)
		return
	}

	issuesByAgeCategory["older than 1mth"] = append(
		issuesByAgeCategory["older than 1mth"], i)

	return
}

func main() {
	if len(os.Args[1:]) > 0 {
		results, err := github.SearchIssues(os.Args[1:])
		if err != nil {
			log.Fatal(err)
		}
		for _, i := range results.Items {
			addIssue(i)
		}
		for k, v := range issuesByAgeCategory {
			fmt.Println(k)
			for _, i := range v {
				fmt.Printf("  %s\n", i.Title)
			}
		}
	}
}
