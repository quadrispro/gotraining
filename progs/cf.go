package main

import (
	"ch2/tempconv"
	"fmt"
	"os"
	"strconv"
)

var bitSize int = 64

func main() {
	var f tempconv.Fahrenheit
	var k tempconv.Kelvin
	var c tempconv.Celsius

	for _, arg := range os.Args[1:] {
		t, err := strconv.ParseFloat(arg, 64)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s: %v\n", os.Args[0], err)
		}
		f = tempconv.Fahrenheit(t)
		k = tempconv.Kelvin(t)
		c = tempconv.Celsius(t)
		fmt.Printf(
			"%s = %s\n%s = %s\n%s = %s\n%s = %s\n%s = %s\n%s = %s\n",
			f, tempconv.FToC(f), c, tempconv.CToF(c),
			k, tempconv.KToC(k), c, tempconv.CToK(c),
			k, tempconv.KToF(k), f, tempconv.FToK(f),
		)
	}
}
