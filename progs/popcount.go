package main

import (
	"ch2/popcount"
	"fmt"
	"os"
	"strconv"
)

func main() {
	for _, arg := range os.Args[1:] {
		x, err := strconv.ParseUint(arg, 10, 64)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s: %v\n", os.Args[0], err)
			os.Exit(1)
		}

		fmt.Printf("%d :=> %d\n", x, popcount.PopCount(x))
		fmt.Printf("%d :=> %d\n", x, popcount.PopCountLoop(x))
	}
}
