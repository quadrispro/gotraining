package basename

import (
	"fmt"
	"strings"
)

func Basename(s, suffix string) string {
	// Don't make sense process empty strings
	if len(s) == 0 {
		return s
	}
	// In case it was a slash-terminated directory name e.g. /path/
	if lastRuneIndex := len(s) - 1; s[lastRuneIndex] == '/' {
		s = s[:lastRuneIndex]
	}
	// Strip out the prefix
	lastSlash := strings.LastIndex(s, "/")
	s = s[lastSlash+1:]
	// Strip suffix if there's any
	sLength := len(s)
	if suffixLength := len(suffix); sLength > suffixLength &&
		s[sLength-suffixLength:] == suffix {
		s = s[:sLength-suffixLength]
	}
	return s
}
