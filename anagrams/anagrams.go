package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"
)

func fatal(err int, s string) {
	fmt.Fprintf(os.Stderr, "%v\n", s)
	os.Exit(err)
}

func anagrams(s1, s2 string) bool {
	var buf bytes.Buffer

	for _, char := range s1 {
		if j := bytes.IndexRune(buf.Bytes(), char); j == -1 {
			sep := string(char)
			if strings.Count(s1, sep) != strings.Count(s2, sep) {
				return false
			}
			buf.WriteRune(char)
		}
	}

	return true
}

func main() {
	if len(os.Args) != 3 {
		fatal(1, "Too few arguments")
	}
	fmt.Println(anagrams(os.Args[1], os.Args[2]))
}
