// Accept a floating point number and display it comma-separated
package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func comma(s string) (string, error) {
	const places int = 3
	var buf bytes.Buffer
	var offset, startCur, endCur, sLength int

	sLength = len(s)
	endCur = sLength
	if endCur == 0 {
		return s, nil
	}
	if point := strings.Index(s, "."); point != -1 {
		endCur = point
	}
	if sign := s[0]; sign == '+' || sign == '-' {
		buf.WriteByte(sign)
		offset++
		startCur++
		endCur--
	}

	offset += endCur % places

	if offset > startCur {
		if _, err := buf.WriteString(s[startCur:offset]); err != nil {
			return s, err
		}
	}
	for ; offset < endCur; offset += places {
		if offset-startCur != 0 {
			buf.WriteRune(',')
		}
		buf.WriteString(s[offset : offset+places])
	}
	if sLength > endCur+1 {
		buf.WriteString(s[endCur+1 : sLength])
	}

	return buf.String(), nil
}

func main() {
	ProgName := filepath.Base(os.Args[0])
	for _, arg := range os.Args[1:] {
		if _, err := strconv.ParseFloat(arg, 64); err != nil {
			fmt.Fprintf(os.Stderr, "%s: error -- %v\n", ProgName, err)
		} else {
			if sWithCommas, err := comma(arg); err != nil {
				fmt.Fprintf(os.Stderr, "%s: error -- %v\n", ProgName, err)
			} else {
				fmt.Println(sWithCommas)
			}
		}
	}
}
