package main

import (
	"fmt"
	"unicode"
)

const asciiSpace byte = byte(' ')

func squashs(bs []byte) []byte {
	for i, b := range bs {
		if i > 1 && unicode.IsSpace(rune(b)) && unicode.IsSpace(rune(bs[i-1])) {
			bs[i-1] = asciiSpace
			return squashs(append(bs[:i], bs[i+1:]...))
		}
	}
	return bs
}

func main() {
	s := []byte("ciao alessio   come    stai  ?")
	fmt.Println(string(s))
	s = squashs(s)
	fmt.Println(string(s))
}
