package main

import (
	"fmt"
)

func groadups(s []string) []string {
	if len(s) > 1 {
		for i, cur := range s {
			if i > 0 && cur == s[i-1] {
				return append(s[:i], groadups(s[i+1:])...)
			}
		}
	}
	return s
}

func main() {
	s := []string{"ciao", "alessio", "alessio", "come", "come", "stai", "alessio", "?"}
	fmt.Println(s)
	s = groadups(s)
	fmt.Println(s)
}
