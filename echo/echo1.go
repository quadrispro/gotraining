package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	sep := " "
	fmt.Println(strings.Join(os.Args[1:], sep))
}
p.