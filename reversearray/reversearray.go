package main

import (
	"fmt"
)

func reverse(array *[8]int) {
	for i, j := 0, len(array)-1; i < j; i, j = i+1, j-1 {
		array[i], array[j] = array[j], array[i]
	}
}

func main() {
	array := [...]int{134, 0, -7, 45, 78, 2666, -829348, 23131}
	fmt.Println(array)
	reverse(&array)
	fmt.Println(array)
}
