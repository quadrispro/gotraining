package main

import (
	"fmt"
)

func lrotate(s []int) {
	length := len(s)
	if length > 1 {
		first := s[0]
		copy(s[0:length-1], s[1:])
		s[length-1] = first
	}
}

func rrotate(s []int) {
	length := len(s)
	if length > 1 {
		last := s[length-1]
		copy(s[1:length], s[:length-1])
		s[0] = last
	}
}

func main() {
	s := []int{134, 0, -7, 45, 78, 2666, -829348, 23131}
	fmt.Println(s)
	lrotate(s)
	fmt.Println(s)
	rrotate(s)
	fmt.Println(s)
}
