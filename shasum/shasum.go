package main

import (
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"hash"
	"io"
	"os"
)

func main() {
	var h hash.Hash
	algo := flag.Int("t", 0, "0: sha256 - 1: sha384 - 2: sha512")
	flag.Parse()
	switch *algo {
	case 0:
		h = sha256.New()
	case 1:
		h = sha512.New384()
	case 2:
		h = sha512.New()
	default:
		fmt.Fprintf(os.Stderr, "Wrong argument -- %d\n", *algo)
		os.Exit(1)
	}

	if _, err := io.Copy(h, os.Stdin); err != nil {
		fmt.Fprintf(os.Stderr, "%v", err)
		os.Exit(2)
	}
	fmt.Printf("%x\n", h.Sum(nil))
}
