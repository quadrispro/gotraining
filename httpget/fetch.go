package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func abort(err error, exitCode int) {
	fmt.Fprintf(os.Stderr, "%s: %v\n", os.Args[0], err)
	os.Exit(exitCode)
}

func main() {
	for _, url := range os.Args[1:] {
		if !strings.HasPrefix(url, "http://") {
			url = "http://" + url
		}
		resp, err := http.Get(url)
		if err != nil {
			abort(err, 1)
		}

		fmt.Printf("FETCH %s: %s\n", url, resp.Status)
		_, err = io.Copy(os.Stdout, resp.Body)
		resp.Body.Close()
		if err != nil {
			abort(err, 2)
		}
	}
}
