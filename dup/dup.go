package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func countLines(f *os.File, counts map[string]int) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
	}
}

func analyzeCounts(counts map[string]int) {
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}
}

func main2() {
	args := os.Args[1:]
	counts := make(map[string]int)

	if len(args) == 0 {
		countLines(os.Stdin, counts)
	} else {
		for _, arg := range args {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s: %v\n", os.Args[0], err)
				continue
			}
			countLines(f, counts)
			f.Close()
		}
	}

	analyzeCounts(counts)
}

func main3() {
	counts := make(map[string]int)

	for _, filename := range os.Args[1:] {
		data, err := ioutil.ReadFile(filename)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s: %v\n", os.Args[0], err)
			continue
		}
		for _, line := range strings.Split(string(data), "\n") {
			counts[line]++
		}
	}

	analyzeCounts(counts)
}

func main4() {
	counts := make(map[string]int)
	files := make(map[string]map[string]int)

	for _, filename := range os.Args[1:] {
		data, err := ioutil.ReadFile(filename)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s: %v\n", os.Args[0], err)
			continue
		}
		for _, line := range strings.Split(string(data), "\n") {
			counts[line]++
			if files[line] == nil {
				files[line] = make(map[string]int)
			}
			files[line][filename]++
		}
	}

	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%s:", line)
			for filename, _ := range files[line] {
				fmt.Printf(" %s", filename)
			}
			fmt.Println()
		}
	}
}

func main() {
	main4()
}
