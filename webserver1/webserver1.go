package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
)

var (
	mu    sync.Mutex
	count int
)

func handler(w http.ResponseWriter, req *http.Request) {
	mu.Lock()
	count++
	mu.Unlock()
	fmt.Fprintf(w, "%s %s %s\n", req.Method, req.URL, req.Proto)
	// Send the header back
	for k, v := range req.Header {
		fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
	}
	fmt.Fprintf(w, "Host: %q\n", req.Host)
	fmt.Fprintf(w, "RemoteAddr: %q\n", req.RemoteAddr)
	// Send back the form data too
	if err := req.ParseForm(); err != nil {
		log.Print(err)
	}
	for k, v := range req.Form {
		fmt.Fprintf(w, "Form[%q] = %q\n", k, v)
	}
}

func counter(w http.ResponseWriter, req *http.Request) {
	mu.Lock()
	fmt.Fprintf(w, "Count of requests received: %d\n", count)
	mu.Unlock()
}

func main() {
	address := "0.0.0.0"
	port := 8000

	switch len(os.Args[1:]) {
	case 2:
		if p, err := strconv.Atoi(os.Args[2]); err != nil {
			log.Fatalln("Port has a wrong format: %q", os.Args[2])
			os.Exit(1)
		} else {
			port = p
		}
		fallthrough
	case 1:
		address = os.Args[1]
	default:
		break
	}

	http.HandleFunc("/", handler)
	http.HandleFunc("/count", counter)

	log.Println(fmt.Sprintf("Listening on %s (port %d)", address, port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%d", address, port), nil))
}
